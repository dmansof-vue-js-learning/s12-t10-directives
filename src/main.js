import Vue from 'vue'
import App from './App.vue'

// customdirective: my-on
Vue.directive('myon', {
  bind(el, bindings, vnode) {
    if (bindings.arg == 'click') {
      el.addEventListener('click', function() {
        console.debug('Button clicked alert');
      });
    }
  }
})

// customdirective: my-customOn
Vue.directive('customOn', {
  bind(el, bindings, vnode) {
    console.debug('bind');
    if (bindings.arg == 'mouseenter') {
      console.debug(bindings.arg, bindings.value);
      el.addEventListener(bindings.value, function() {
        console.debug('mouse enter');
      });
      console.debug('el', el);
    } else if (bindings.arg == 'mouseleave') {
      el.addEventListener(bindings.value, function() {
        console.debug('mouse leave');
      });
    } else if (bindings.arg == 'mouseover') {
      el.addEventListener(bindings.value, function() {
        console.debug('mouse over');
      });
    }
  }
})

new Vue({
  el: '#app',
  render: h => h(App)
})
